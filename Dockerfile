FROM ruby:2.3.0
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /tuto
WORKDIR /tuto
ADD Gemfile /tuto/Gemfile
ADD Gemfile.lock /tuto/Gemfile.lock
RUN bundle install
ADD . /tuto